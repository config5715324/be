# Models
from ..models.mypt_config_group import *
from ..models.mypt_config import *
# Serializers
from rest_framework.serializers import *
from ..serializers.mypt_config_group_serializer import *
from ..serializers.mypt_config_serializer import *
# Helpers
from ...my_core.helpers.response import *
from ...configs.response_codes import *
import re
EMAIL_REGEX = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

# quan ly group
def validation_group_id(group_ID):
    if group_ID is None:
        return response_data(None, 0, "can co group_ID")
    if not isinstance(group_ID, int):
        return response_data(None, 0, "group_ID phai la integer")
    if group_ID < 0:
        return response_data(None, 0, "group_ID phai la so nguyen duong")
        
    return None

def validation_group_add(data):
    
    group_title = data.get('group_title')
    if not group_title:
        return response_data(None, 0 , "can co group_title")
    
    email_created = data.get('email_created')
    if not email_created:
        return response_data(None, 0 , "can co email")
    if not re.match(EMAIL_REGEX, email_created):
        return response_data(None, 0, "dinh dang email khong hop le")
    
    return None

def validation_group_update(data):
    
    if 'group_ID' in data:
        group_ID = data['group_ID']
        if not MyPTConfigGroup.objects.filter(group_ID=group_ID).exists():
            return response_data({}, statusCode=0, message="group_ID khong ton tai") 
    
    # kiem tra dieu kien neu co cap nhat moi
    if 'group_title' in data:
        group_title = data['group_title']
        if not group_title:
            return response_data(None, 0, "can phai co group_title")
        
    if 'email_created' in data:
        email_created = data['email_created']
        if not email_created:
            return response_data(None, 0, "can co email")
        if not re.match(EMAIL_REGEX, email_created):
            return response_data(None, 0, "email khong dung dinh dang")

    return None


# quan ly config
def validation_config_add(data):
    
    group_ID = data.get('group_ID')
    validate_groupID = validation_group_id(group_ID)
    if validate_groupID:
        return validate_groupID
    if not MyPTConfigGroup.objects.filter(group_ID=group_ID).exists():
        return response_data({}, 0, "group_ID khong ton tai")
    
    config_type = data.get('config_type')
    if not config_type:
        return response_data({}, statusCode=0, message="can co config_type")
    if config_type not in [item[0] for item in MyPTConfig.TYPE_VALUES]:
        return response_data({}, statusCode=0, message="config_type khong hop le")

    config_key = data.get('config_key')
    if not config_key:
        return response_data({}, statusCode=0, message="can co config_key")
    if MyPTConfig.objects.filter(config_key=config_key).exists():
        return response_data({}, statusCode=0, message="config_key da ton tai")
    
    config_value = data.get('config_value')
    if not config_value:
        return response_data({}, 0, "can co value")

    config_status = data.get('config_status', 'enabled')
    if config_status not in [item[0] for item in MyPTConfig.STATUS_VALUES]:
        return response_data({}, statusCode=0, message='config_status khong hop le')

    owner = data.get('owner')
    if not owner:
        return response_data({}, statusCode=0, message='can co owner')
    if not re.match(EMAIL_REGEX, owner):
        return response_data({}, statusCode=0, message="dinh dang email khong hop le")

    note = data.get('note')
    if not note:
        return response_data({}, statusCode=0, message='can co note')

    return None

def validation_config_update(data):
    config_ID = data.get('config_ID')
    if not config_ID:
        return response_data(None, 0, "can co config_ID")
    if not isinstance(config_ID, int):
        return response_data(None, 0, "config_ID phai la integer")
    if config_ID < 0:
        return response_data(None, 0, "config_ID phai la nguyen duong")

    # kiem tra dieu kien neu co cap nhat moi
    if 'group_ID' in data:
        group_ID = data['group_ID']
        if not group_ID:
            return response_data({}, statusCode=0, message="can co group_ID")
        validate_groupID = validation_group_id(group_ID)
        if validate_groupID:
            return validate_groupID
        if not MyPTConfigGroup.objects.filter(group_ID=group_ID).exists():
            return response_data({}, statusCode=0, message="group_ID khong ton tai")
    
    if 'config_type' in data:
        config_type = data['config_type']
        if config_type not in [ item[0] for item in MyPTConfig.TYPE_VALUES]:
            return response_data({} , statusCode=0, message="config_type khong hop le")   

    if 'config_key' in data:
        config_key = data['config_key']
        if MyPTConfig.objects.exclude(config_ID=config_ID).filter(config_key=config_key).exists():
            return response_data({}, statusCode=0, message="config_key da ton tai")
        
    if 'config_value' in data:
        config_value = data['config_value']
        if not config_value:
            return response_data({}, 0, "can co value")

    if 'config_status' in data:
        config_status = data['config_status']
        if config_status not in [item[0] for item in MyPTConfig.STATUS_VALUES]:
            return response_data({}, statusCode=0, message='config_status khong hop le')

    if 'owner' in data:
        owner = data['owner']
        if not owner:
            return response_data({}, statusCode=0, message='can co owner')
        if not re.match(EMAIL_REGEX, owner):
            return response_data({}, statusCode=0, message="dinh dang email khong hop le")

    if 'note' in data:
        note = data['note']
        if not note:
            return response_data({}, statusCode=0, message='can co note')

    return None

def validation_config_remove(config_ID):
    
    if not config_ID or config_ID==None:
        return response_data(None, 0, "can co config_ID")
    if not isinstance(config_ID, int):
        return response_data(None, 0, "config_ID phai la integer")
    if config_ID < 0:
        return response_data(None, 0, "config_ID phai la nguyen duong")
    
    try:
        config = MyPTConfig.objects.get(config_ID=config_ID)
    except MyPTConfig.DoesNotExist:
        return response_data({}, statusCode=0, message="config_ID khong ton tai")

    return None