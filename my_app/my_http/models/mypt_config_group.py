from django.db import models

class MyPTConfigGroup(models.Model):
    class Meta:
        db_table = 'mypt_managers_group_configs'

    group_ID = models.AutoField(primary_key=True)
    group_title = models.CharField(max_length=100)
    group_key = models.TextField(null=True, blank=True)
    email_created = models.CharField(max_length=100, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.group_title
        # return "{} ({}) ({})".format(self.group_ID, self.group_title, self.group_status)