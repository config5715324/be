# Models
from ..models.mypt_config_group import *
from ..models.mypt_config import *
# Serializers
from ..serializers.mypt_config_group_serializer import *
from ..serializers.mypt_config_serializer import *
# Helpers
from rest_framework.viewsets import ViewSet
from ...my_core.helpers.response import *
from ..paginations.custom_pagination import StandardPagination
from ..validations.manager_config_validation import *
from ...configs.response_codes import *
# from ...throttles.throttling_api_time import *
from my_project.throttling import *

class ManagerGroupConfig(ViewSet):
    throttle_classes = []

    def get_throttles(self):
        # Các api cần chặn request
        if self.action in [
            # 'post_do_checkin',
            # 'get_emp_checkin_info'
        ]:
            return [UserThrottle()]
        return self.throttle_classes
    
    # danh sach cac group config
    def groupShow(self, request):
        queryset = MyPTConfigGroup.objects.all()
        if not queryset.exists():
            return response_data({}, statusCode=STATUS_CODE_NO_DATA, message=MESSAGE_API_NO_DATA)
        serializer = MyPTConfigGroupSerializerShow(queryset, many=True)
        return response_data(serializer.data)
    
    # them group
    def groupAdd(self, request):
        try:
            data = request.data
                        
            # kiem tra fields
            validate_group_add = validation_group_add(data)
            if validate_group_add:
                return validate_group_add
            
            serializer = MyPTConfigGroupSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return response_data({}, 1, "group them thanh cong")
            return response_data(serializer.errors, 0, "du lieu khong hop le")
            
        except Exception as e:
            return response_data({}, 0, str(e))
    
    # sua group
    def groupUpdate(self, request):
        try:
            data = request.data
            # toi ID can update
            group_ID = data.get('group_ID')
            validate_groupID = validation_group_id(group_ID)
            if validate_groupID:
                return validate_groupID
            
            # kiem tra fields
            validate_group_update = validation_group_update(data)
            if validate_group_update:
                return validate_group_update
            
            group = MyPTConfigGroup.objects.get(group_ID=group_ID)
            serializer = MyPTConfigGroupSerializer(instance=group, data=data, partial= True)
            if serializer.is_valid():
                serializer.save()
                return response_data(serializer.data, 1, "cap nhat group thanh cong")
            return response_data(serializer.errors, 0, "du lieu khong hop le")
        
        except MyPTConfigGroup.DoesNotExist:
            return response_data({}, 0, "group_ID khong ton tai")
        except Exception as e:
            return response_data({}, 0, str(e))
    
    # xoa group
    def groupRemove(self, request):
        try:
            data = request.data
            group_ID = data.get('group_ID')
            validate_groupID = validation_group_id(group_ID)
            if validate_groupID:
                return validate_groupID
            
            if MyPTConfig.objects.filter(group_ID=group_ID).exists():
                return response_data({}, 0, "khong the xoa group vi co ton tai config")
            
            group = MyPTConfigGroup.objects.get(group_ID=group_ID)
            group.delete()
            return response_data({}, 1, "xoa group thanh cong")
    
        except MyPTConfigGroup.DoesNotExist:
            return response_data({}, 0, "group does not exists")
        except Exception as e:
            return response_data({}, 0, str(e))
