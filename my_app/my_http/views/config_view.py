# Models
from ..models.mypt_config_group import *
from ..models.mypt_config import *
# Serializers
from ..serializers.mypt_config_group_serializer import *
from ..serializers.mypt_config_serializer import *
# Helpers
from rest_framework.viewsets import ViewSet
from ...my_core.helpers.response import *
from ..paginations.custom_pagination import StandardPagination
from ..validations.manager_config_validation import *
from ...configs.response_codes import *
# from ...throttles.throttling_api_time import *
from my_project.throttling import *
        
class ManagerConfig(ViewSet):
    throttle_classes = []

    def get_throttles(self):
        # Các api cần chặn request
        if self.action in [

        ]:
            return [UserThrottle()]
        return self.throttle_classes

    # show danh sach config dua tren group
    def configShow(self, request):
        try:
            data = request.data
            group_ID = data.get('group_ID')
            validate_groupID = validation_group_id(group_ID)
            if validate_groupID:
                return validate_groupID
            
            try:
                group = MyPTConfigGroup.objects.get(group_ID=group_ID)
            except MyPTConfigGroup.DoesNotExist:
                return response_data({}, statusCode=0, message="group_ID khong ton tai")
            
            queryset = MyPTConfig.objects.filter(group_ID=group_ID)
            if not queryset.exists():
                return response_data({}, statusCode=0, message=f"'{group.group_title}' khong ton tai config")
            
            
            queryset = MyPTConfig.objects.filter(group_ID=group_ID)
            # phan trang
            page_size = request.query_params.get('page_size', 10)
            paginator = StandardPagination()
            paginator.page_size = page_size
            paginated_queryset = paginator.paginate_queryset(queryset, request)

            serializer = MyPTConfigSerializer(paginated_queryset, many=True)
            total = queryset.count()
            data = serializer.data
            count = len(data)

            return response_paginator(total, int(page_size), data, count)
        
        except Exception as e:
            return response_data({}, statusCode=0, message=str(e))

    # them config moi
    def configAdd(self, request):
        try:
            data = request.data
            
            # check fields
            validate_add = validation_config_add(data)
            if validate_add:
                return validate_add

            # add moi
            serializer = MyPTConfigSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return response_data(serializer.data, statusCode=1, message="config them thanh cong")
            return response_data(serializer.errors, statusCode=0, message="du lieu khong hop le")

        except Exception as e:
            return response_data({}, statusCode=0, message=str(e))
        
        
    # thay doi config
    def configUpdate(self, request):
        try:
            data = request.data
            # check 
            validate_update = validation_config_update(data)
            if validate_update:
                return validate_update

            config_ID = data.get('config_ID')
            config = MyPTConfig.objects.get(config_ID=config_ID)
            # serializer
            serializer = MyPTConfigSerializer(instance=config, data=data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return response_data(serializer.data, statusCode=1, message="Config cap nhat thanh cong")
            return response_data(serializer.errors, statusCode=0, message="du lieu khong hop le")
        
        except MyPTConfig.DoesNotExist:
            return response_data({}, 0, "config_ID khong ton tai")
        except Exception as e:
            return response_data({}, statusCode=0, message=str(e))


    # xoa config
    def configRemove(self, request):
        try:
            data = request.data
            config_ID = data.get('config_ID')
            
            # check
            validate_remove = validation_config_remove(config_ID)
            if validate_remove:
                return validate_remove

            config = MyPTConfig.objects.get(config_ID=config_ID)
            config.delete()
            return response_data({}, statusCode=1, message="Config xoa thanh cong")

        except Exception as e:
            return response_data({}, statusCode=0, message=str(e))
        