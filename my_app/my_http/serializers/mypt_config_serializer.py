from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from ..models.mypt_config import MyPTConfig
from ...configs.config_table import *

class MyPTConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = MyPTConfig
        fields = '__all__'
    
    def to_representation(self, instance):
        representation = super().to_representation(instance)

        for field, vietnamese_name in config_table.items():
            representation[vietnamese_name] = representation.pop(field)
        
        return representation