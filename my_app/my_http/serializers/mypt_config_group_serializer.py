from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from ..models.mypt_config_group import MyPTConfigGroup

class MyPTConfigGroupSerializerShow(serializers.ModelSerializer):
    class Meta:
        model = MyPTConfigGroup
        fields = ['group_ID', 'group_title', 'group_key']
        # fields = ['group_ID', 'group_title', 'group_key', 'email_created']

class MyPTConfigGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = MyPTConfigGroup
        fields = '__all__'