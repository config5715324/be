config_table = {
    'config_ID': 'ID',
    'config_type': 'Loại',
    'config_key': 'Mã Khóa',
    'group_ID': 'ID nhóm',
    'config_value': 'Giá trị',
    'config_description_vi': 'Mô tả tiếng Việt',
    'config_description_en': 'Mô tả tiếng Anh',
    'config_status': 'Trạng thái',
    'owner': 'Người tạo',
    'date_created': 'Ngày tạo',
    'date_last_updated': 'Ngày cập nhật',
    'note': 'Ghi chú',
}
