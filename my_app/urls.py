from django.urls import path
from .my_http.views.config_view import *
from .my_http.views.group_view import *

urlpatterns = [
    
    # quan ly group config
    path('group-show', ManagerGroupConfig.as_view({'post': 'groupShow'})),
    path('group-add', ManagerGroupConfig.as_view({'post' : 'groupAdd'})),
    path('group-update', ManagerGroupConfig.as_view({'post' : 'groupUpdate'})),
    path('group-remove', ManagerGroupConfig.as_view({'post' : 'groupRemove'})),

    # quan ly config
    path('config-show', ManagerConfig.as_view({'post': 'configShow'})),
    path('config-add', ManagerConfig.as_view({'post': 'configAdd'})),
    path('config-update', ManagerConfig.as_view({'post': 'configUpdate'})),
    path('config-remove', ManagerConfig.as_view({'post': 'configRemove'})),
    
]
