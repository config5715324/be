import time
from functools import wraps

minimum_interval = 10  # Thời gian tối thiểu giữa mỗi lần gọi API (5 giây)

def minimum_interval_global(view_func):
    last_call = 0  # Add local variable assignment for 'last_call'
    
    @wraps(view_func)
    def _wrapped_view(*args, **kwargs):
        nonlocal last_call

        current_time = time.time()

        if current_time - last_call < minimum_interval:
            time.sleep(minimum_interval - (current_time - last_call))

        last_call = time.time()
        return view_func(*args, **kwargs)

    return _wrapped_view
