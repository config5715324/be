import uuid
from django.conf import settings as project_settings
from my_app.my_http.models.ho_oauth_access_token import HoOauthAccessToken
from my_app.my_http.models.ho_oauth_refresh_token import HoOauthRefreshToken
from datetime import datetime
from datetime import timedelta
from my_app.configs import app_settings
from my_app.my_core.Entities.my_jwt import MyJwt
import redis
from my_app.my_core.Entities.centralized_session import CentralizedSession

from my_app.my_core.helpers.utils import is_null_or_empty
from my_app.my_http.Entities.oauth_access_token_handler import OauthAccessTokenHandler
from my_app.my_http.Entities.oauth_refresh_token_handler import OauthRefreshTokenHandler

from my_app.my_http.Apis.mypt_ho_profile_apis import MyPtHoProfileApis
from my_app.my_http.Entities.permission_handler import *

# CODE GHI LOG
# from my_app.my_http.serializers.log_user_serializer import LogUserSerializer


class TokenHandler:
    def genTokenByUser(self, userInfo, authData, clientGrant):
        jwtObj = MyJwt()

        userId = int(userInfo.get("user_id"))
        caseRefreshToken = False
        refreshTokenId = ""
        if authData.get("refreshTokenId", None) is not None:
            caseRefreshToken = True
            refreshTokenId = str(authData.get("refreshTokenId"))
            # TAM THOI : revoke het cac Access Token theo userId va refreshTokenId nay de chuan bi tao Access Token moi
            # self.revokeAccessTokensByRefreshToken(userId, refreshTokenId)
            # TODO: revoke het cac Access Token & Refresh Token hien tai cua userId nay (tru refreshTokenId ra) neu muon tai 1 thoi diem, 1 email chi login tren 1 device duy nhat
            # self.revokeTokens(userId, True, refreshTokenId)
            revokeTokensResult = self.revokeTokens(userId, True, refreshTokenId)
        else:
            # TODO: revoke het cac Access Token & Refresh Token hien tai cua userId nay neu muon tai 1 thoi diem, 1 email chi login tren 1 device duy nhat
            # self.revokeTokens(userId, False, "")
            revokeTokensResult = self.revokeTokens(userId, False, "")

        if not revokeTokensResult["isRevokeToken"]:
            # curDt = datetime.now()
            # accessTokenExpiredDt = curDt + timedelta(minutes=app_settings.EXPIRES_AT_ACCESS_TOKEN)
            # accessTokenExpiredDt = curDt + timedelta(minutes=app_settings.EXPIRES_AT_ACCESS_TOKEN)
            return {
                "accessToken": jwtObj.createJtiToken(revokeTokensResult["accessTokenId"], {
                    "issuedAt": int(revokeTokensResult["createAt"].timestamp()),
                    "expiration": int(revokeTokensResult["expiresAt"].timestamp())
                }),
                "refreshToken": authData["jwtRefreshToken"],
                "jti": revokeTokensResult["accessTokenId"]
            }

        # TAO access token
        accessTokenModel = HoOauthAccessToken()
        accessTokenModel.id = self.genUuidForToken("createAccessToken", userId)
        newAccessTokenId = accessTokenModel.id

        accessTokenModel.user_id = userId
        accessTokenModel.client_id = str(clientGrant.get("client_id"))
        # Neu day la case "Tao lai Access Token tu Refresh Token"
        if caseRefreshToken == True:
            # print("case refresh token 123")
            accessTokenModel.refresh_token_id = refreshTokenId
        # set cac field con lai
        # accessTokenModel.device_id = userInfo.get("device_id")
        # accessTokenModel.device_name = userInfo.get("device_name")
        # accessTokenModel.device_token = userInfo.get("device_token")
        # accessTokenModel.device_platform = userInfo.get("device_platform")
        accessTokenModel.grant_id = int(clientGrant.get("grant_id"))
        accessTokenModel.revoked = 0

        # set expires_at
        curDt = datetime.now()
        accessTokenModel.created_at = curDt.strftime("%Y-%m-%d %H:%M:%S")
        accessTokenExpiredDt = curDt + timedelta(minutes=app_settings.EXPIRES_AT_ACCESS_TOKEN)
        accessTokenModel.expires_at = accessTokenExpiredDt.strftime("%Y-%m-%d %H:%M:%S")
        # save
        resCreateAccessToken = accessTokenModel.save()

        # TAO refresh token neu day la case "Tao Access Token moi" (khong danh cho case "Tao lai Access Token tu Refresh Token")
        if caseRefreshToken == False:
            refreshTokenModel = HoOauthRefreshToken()
            refreshTokenModel.id = self.genUuidForToken("createRefreshToken", userId)
            refreshTokenId = refreshTokenModel.id
            refreshTokenModel.user_id = userId
            refreshTokenModel.access_token_id = newAccessTokenId
            refreshTokenModel.client_id = str(clientGrant.get("client_id"))
            refreshTokenModel.revoked = 0
            refreshTokenExpiredDt = curDt + timedelta(minutes=app_settings.EXPIRES_AT_REFRESH_TOKEN)
            refreshTokenModel.expires_at = refreshTokenExpiredDt.strftime("%Y-%m-%d %H:%M:%S")
            resCreateRefreshToken = refreshTokenModel.save()
            # lay refresh token moi tao de update vao cot refresh_token_id cua bang oauth_access_tokens
            HoOauthAccessToken.objects.filter(id=newAccessTokenId).update(refresh_token_id=refreshTokenId,
                                                                          updated_at=datetime.now().strftime(
                                                                              "%Y-%m-%d %H:%M:%S"))
        else:
            # update lai cot access_token_id voi newAccessTokenId cho row oauth_refresh_tokens
            HoOauthRefreshToken.objects.filter(id=refreshTokenId).update(access_token_id=newAccessTokenId,
                                                                         updated_at=datetime.now().strftime(
                                                                             "%Y-%m-%d %H:%M:%S"))

        # TODO: Luu vao redis, voi key là AccessTokenId
        if authData.get("isSaveCentralizedStorage") == True:
            resSaveCenSession = self.saveCentralizedSession({
                "id": newAccessTokenId,
                "expiresAt": accessTokenExpiredDt
            }, clientGrant, userInfo, authData)
            if resSaveCenSession.get("resSave") == False:
                return None

        # CODE GHI LOG
        # sessionData = resSaveCenSession['sessionData']
        # sessionData['functionCode'] = 'LOGIN_AZURE'
        # sessionData['functionName'] = 'login azure'
        # sessionData['actionCode'] = 'DANG_NHAP'
        # sessionData['actionName'] = 'đăng nhập bằng account công ty'
        # sessionData['serviceName'] = 'mypt-ho-auth-api'
        # sessionData['webBrowser'] = None
        # sessionData['apiInput'] = None
        # data_log = LogUserSerializer(data=sessionData)
        # if not data_log.is_valid():
        #     pass
        # data_log.save()

        # return
        return {
            "accessToken": jwtObj.createJtiToken(newAccessTokenId, {
                "issuedAt": int(curDt.timestamp()),
                "expiration": int(accessTokenExpiredDt.timestamp())
            }),
            "refreshToken": jwtObj.createJtiToken(refreshTokenId),
            "jti": newAccessTokenId
        }

    # revoke het cac Access Token & Refresh Token hien tai cua userId truyen vao
    # Neu day la case "Refresh token" (caseRefreshToken = True) : ko xoa row oauth_refresh_tokens cua refreshTokenId nay
    def revokeTokens(self, userId, caseRefreshToken=False, refreshTokenId=""):
        # Lay ra cac access token (voi revoked = 0) cua userId nay
        oatHandler = OauthAccessTokenHandler()
        accessTokenObjs = oatHandler.getUnRevokedAccessTokensByUser(userId)

        if caseRefreshToken and refreshTokenId:
            accessTokenObjs = [x for x in accessTokenObjs if x["refresh_token_id"] == refreshTokenId]
            if len(accessTokenObjs) > 0:
                accessTokenObj = accessTokenObjs[0]
                # Kiem tra trong redis co ton tai
                redisInstance = redis.StrictRedis(host=project_settings.REDIS_HOST_CENTRALIZED,
                                                  port=project_settings.REDIS_PORT_CENTRALIZED,
                                                  db=project_settings.REDIS_DATABASE_CENTRALIZED,
                                                  password=project_settings.REDIS_PASSWORD_CENTRALIZED,
                                                  decode_responses=True,
                                                  charset="utf-8")
                user_token_str = redisInstance.get(f'session:{accessTokenObj["id"]}')

                if user_token_str is not None:
                    print(f'>>>>>>>>>>> USER TOKEN: {accessTokenObj["id"]} CON HAN TRONG REDIS VA KHONG TAO MOI')
                    return {
                        "isRevokeToken": False,
                        "accessTokenId": accessTokenObj["id"],
                        "refreshTokenId": accessTokenObj["refresh_token_id"],
                        "createAt": datetime.strptime(accessTokenObj["created_at"], "%Y-%m-%dT%H:%M:%S"),
                        "expiresAt": datetime.strptime(accessTokenObj["expires_at"], "%Y-%m-%dT%H:%M:%S")
                    }

        # revoke cac access token cua userId nay
        resRevokeAccessTokens = oatHandler.revokeAccessTokensByUser(userId)
        # revoke cac refresh token cua userId nay
        # CHU Y : Neu day la case "Refresh token" (caseRefreshToken = True) : ko xoa row oauth_refresh_tokens cua refreshTokenId nay
        ortHandler = OauthRefreshTokenHandler()
        if caseRefreshToken == True and refreshTokenId != "":
            resRevokeRefreshTokens = ortHandler.revokeRefreshTokensByUser(userId, refreshTokenId)
        else:
            resRevokeRefreshTokens = ortHandler.revokeRefreshTokensByUser(userId)

        # xoa het data cac access token cua userId nay trong Redis
        if accessTokenObjs is not None:
            cenSessionObj = CentralizedSession()
            for accessTokenObj in accessTokenObjs:
                cenSessionObj.removeSession(accessTokenObj.get("id"))

        return {
            "isRevokeToken": True,
            "accessTokenId": "",
            "refreshTokenId": "",
            "createAt": "",
            "expiresAt": ""
        }

    def revokeAccessTokensByRefreshToken(self, userId, refreshTokenId):
        print(
            "[revokeAccessTokensByRefreshToken] revoke het cac access token cua refresh token : " + refreshTokenId + " va userId : " + str(
                userId))
        # Lay ra cac access token dang co refreshTokenId nay
        oatHandler = OauthAccessTokenHandler()
        accessTokenObjs = oatHandler.getAccessTokensByRefreshTokenId(userId, refreshTokenId)
        # update revoked thanh 1 trong DB
        resRevokeAccessToken = HoOauthAccessToken.objects.filter(user_id=userId, refresh_token_id=refreshTokenId,
                                                                 revoked=0).update(revoked=1,
                                                                                   updated_at=datetime.now().strftime(
                                                                                       "%Y-%m-%d %H:%M:%S"))
        # TODO: xoa het cac key (la cac access token thuoc refreshTokenId) trong Redis
        cenSessionObj = CentralizedSession()
        for accessTokenObj in accessTokenObjs:
            cenSessionObj.removeSession(accessTokenObj.get("id"))

        return {
            "resRevoke": True
        }

    def saveCentralizedSession(self, accessTokenInfo, clientGrant, userInfo, authData):
        sessionData = {
            "clientId": str(clientGrant.get("client_id")),
            "grantId": int(clientGrant.get("grant_id")),
            "userId": int(userInfo.get("user_id")),
            "email": userInfo.get("email"),
            "fullName": userInfo.get("full_name"),
            "isTinPncEmployee": 0,
            "branch": "",
            "empCode": "",
            "empContractType": "",
            "childDepart": "",
            "parentDepart": "",
            "agency": "",
            "isHOEmp": 0,
            "jobTitle": "",
            "empAvatarImageUrl": "",
            "permissions": {},
            "new_permissions": {},
            "featuresRoles": {}
        }

        # Tap hop cac phong ban (child_depart) ma user nay co quyen tuong tac de gui qua mypt-ho-profile-api de lay parent_depart, branch cua tung child_depart
        perHandler = PermissionHandler()
        persData = perHandler.getAllPermissionsByUser(int(userInfo.get("user_id")))
        privilegesData = perHandler.getPermissionByUserId(int(userInfo.get("user_id"))) # TODO: DISABLE
        permissionsData = persData.get("persData")
        privileges = privilegesData.get("persData") # TODO: DISABLE
        # Goi API ben mypt-ho-profile-api de lay thong tin profile, employee cua user nay
        profileApis = MyPtHoProfileApis()
        profileInfoData = profileApis.getProfileInfo(
            int(userInfo.get("user_id")), 
            userInfo.get("email"),
            userInfo.get("full_name"), 
            {
                "collectedSpecificChildDeparts": persData.get("collectedSpecificChildDeparts", [])
            }
        )

        # DATA DEMO
        # profileInfoData = {
        #     "isTinPncEmployee": 1,
        #     "childDepart": "PDX",
        #     "parentDepart": "PNCHO",
        #     "isHO": 1,
        #     "branch": "PNC",
        #     "empCode": "00262160",
        #     "jobTitle": "CB Kỹ thuật TKBT",
        #     "empContractType": "new",
        #     "empAvatarImageUrl": "",
        #     "branchParentDeparts": {
        #         "TV1HN5TN": {
        #             "chiNhanh": "TV1HN5",
        #             "parentDepart": "TINV1",
        #             "branch": "TIN"
        #         },
        #         "TV1HN6TN": {
        #             "chiNhanh": "TV1HN6",
        #             "parentDepart": "TINV1",
        #             "branch": "TIN"
        #         },
        #         "TV2LCIUR": {
        #             "chiNhanh": "TV2LCI",
        #             "parentDepart": "TINV2",
        #             "branch": "TIN"
        #         },
        #         "TV3HPG1UR": {
        #             "chiNhanh": "TV3HPG",
        #             "parentDepart": "TINV3",
        #             "branch": "TIN"
        #         },
        #         "TV3HPG2UR": {
        #             "chiNhanh": "TV3HPG",
        #             "parentDepart": "TINV3",
        #             "branch": "TIN"
        #         }
        #     },
        #     "featuresRoles": {}
        # }

        if profileInfoData is not None:
            try:

                list_permission_on_job_code = profileInfoData.get("listPermissionIdOnJobCode", [])
                class_info_permission = PermissionHandler()
                dict_info_permission_from_list_permission_id = class_info_permission.get_info_permission_from_list_permission_id(list_permission_on_job_code)
                int_user_id = int(userInfo.get("user_id"))

                if len(list_permission_on_job_code) > 0:
                    for i_per_id in list_permission_on_job_code:
                        dict_data_permission = dict_info_permission_from_list_permission_id.get(i_per_id)

                        class_permission = PermissionHandler()
                        right_on_child_depart_init = class_permission.get_right_on_child_depart_on_permission_id_and_user_id(int_user_id, i_per_id)


                        dict_permission = {
                            "childDepart": profileInfoData.get("rightOnChildDepart", ''),
                            'permissionId': i_per_id,
                            'permissionCode': dict_data_permission.get('permission_code', ''),
                            "userId": int_user_id,
                            'assignMethod': "auto_when_login"
                        }
                        has_right_on_child_depart = dict_data_permission.get("has_depart_right")
                        if has_right_on_child_depart != 1:
                            dict_permission.pop("childDepart")
                        else:
                            if not is_null_or_empty(right_on_child_depart_init):
                                dict_permission.pop("childDepart")

                        list_err = class_permission.provide_permission(dict_permission)
                        print("auto permission >> Error/loi: {}".format(list_err))
                    persData = perHandler.getAllPermissionsByUser(int_user_id)
                    permissionsData = persData.get("persData")
                    privilegesData = perHandler.getPermissionByUserId(int_user_id) # TODO: DISABLE
                    privileges = privilegesData.get("persData") # TODO: DISABLE
            except Exception as ex:
                print("gen token permission >> Error/loi: {}".format(ex))


            sessionData["isTinPncEmployee"] = int(profileInfoData.get("isTinPncEmployee"))
            sessionData["branch"] = str(profileInfoData.get("branch"))
            sessionData["empCode"] = str(profileInfoData.get("empCode"))
            sessionData["empContractType"] = str(profileInfoData.get("empContractType"))
            sessionData["childDepart"] = str(profileInfoData.get("childDepart"))
            sessionData["parentDepart"] = str(profileInfoData.get("parentDepart"))
            sessionData["agency"] = str(profileInfoData.get("agency", ""))
            sessionData["isHOEmp"] = int(profileInfoData.get("isHO"))
            sessionData["jobTitle"] = str(profileInfoData.get("jobTitle"))
            sessionData["empAvatarImageUrl"] = str(profileInfoData.get("empAvatarImageUrl", ""))
            sessionData["featuresRoles"] = profileInfoData.get("featuresRoles", {})
            # xu ly branchParentDeparts
            branchParentDeparts = profileInfoData.get("branchParentDeparts", None)

            if branchParentDeparts is not None:
                for perCode, perData in permissionsData.items():
                    specificChildDeparts = perData.get("specificChildDeparts")

                    if len(specificChildDeparts) > 0:
                        for specificChildDepart in specificChildDeparts:
                            childDepartLevelData = branchParentDeparts.get(specificChildDepart, None)
                            if childDepartLevelData is not None:
                                parent_depart = childDepartLevelData["parentDepart"]
                                chiNhanh = childDepartLevelData["chiNhanh"]
                                branchStr = childDepartLevelData["branch"]
                                if parent_depart not in perData["branch_rights"][branchStr]:
                                    perData["branch_rights"][branchStr].append(parent_depart)
                                if perData["child_depart_rights"].get(parent_depart, None) is None:
                                    perData["child_depart_rights"][parent_depart] = []
                                if specificChildDepart not in perData["child_depart_rights"][parent_depart]:
                                    perData["child_depart_rights"][parent_depart].append(specificChildDepart)

                                perData["all_child_depart_rights"].append({"childDepart": specificChildDepart,
                                                                           "unit": chiNhanh,
                                                                           "parentDepart": parent_depart,
                                                                           "branch": branchStr
                                                                           })

                    # Da su dung xong specificChildDeparts, xoa no khoi perData
                    perData.pop("specificChildDeparts")
            else:
                # print("branchParentDeparts is null, xoa specificChildDeparts trong permissionsData")
                for perCode, perData in permissionsData.items():
                    perData.pop("specificChildDeparts")

        else:
            for perCode, perData in permissionsData.items():
                perData.pop("specificChildDeparts")

        # print(permissionsData)
        sessionData["permissions"] = permissionsData
        sessionData["new_permissions"] = privileges # TODO: DISABLE
        # print("PERMISSION DATA:", permissionsData)

        expiresAtTs = int(accessTokenInfo.get("expiresAt").timestamp())
        seconds = expiresAtTs - int(datetime.now().timestamp())
        # print("so giay cho redis key nay la : " + str(seconds))

        cenSessionObj = CentralizedSession()
        resSaveSession = cenSessionObj.saveSession(accessTokenInfo.get("id"), sessionData, seconds)

        # CODE GHI LOG
        # resSaveSession['sessionData'] = sessionData

        return resSaveSession

    def genUuidForToken(self, tokenType, userId):
        uuidStr = uuid.uuid4()
        return str(uuidStr)
