DROP DATABASE IF EXISTS `mypt_notification_stag_db`;
CREATE DATABASE IF NOT EXISTS `mypt_notification_stag_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `mypt_notification_stag_db`;


DROP TABLE IF EXISTS `mypt_managers_group_configs`;
CREATE TABLE IF NOT EXISTS `mypt_managers_group_configs` (
  `group_ID` int(10) NOT NULL AUTO_INCREMENT,
  `group_title` varchar(100) NOT NULL,
  `group_key` text DEFAULT NULL,
  `email_created` varchar(100) NOT NULL COMMENT 'email người insert',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`group_ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `mypt_managers_group_configs` 
(`group_ID`, `group_title`, `group_key`, `email_created`, `created_at`, `updated_at`) 
VALUES
(1, 'Group A', 'key1', 'creatorA@example.com', NOW(), NOW()),
(2, 'Group B', 'key2', 'creatorB@example.com', NOW(), NOW()),
(3, 'Group C', 'key3', 'creatorC@example.com', NOW(), NOW());

-- select * from mypt_managers_group_configs;
-- select * from mypt_managers_configs;

DROP TABLE IF EXISTS `mypt_managers_configs`;
CREATE TABLE IF NOT EXISTS `mypt_managers_configs` (
  `config_ID` int(10) NOT NULL AUTO_INCREMENT,
  `config_type` enum('constant','message','remote') NOT NULL,
  `config_key` varchar(100) NOT NULL,
  `group_ID` int(10) NOT NULL,
  `config_value` text NOT NULL,
  `config_description_vi` text DEFAULT NULL,
  `config_description_en` text DEFAULT NULL,
  `config_status` enum('disabled','enabled','deleted') NOT NULL DEFAULT 'enabled',
  `owner` varchar(100) NOT NULL COMMENT 'email người insert',
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  `date_last_updated` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `note` varchar(255) NOT NULL COMMENT 'dien giai dong config nay de lam gi',
  PRIMARY KEY (`config_ID`) USING BTREE,
  UNIQUE KEY `config_key` (`config_key`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `mypt_managers_configs` 
(`config_ID`, `config_type`, `config_key`, `group_ID`, `config_value`, `config_description_vi`, `config_description_en`, `config_status`, `owner`, `date_created`, `date_last_updated`, `note`) 
VALUES
(1, 'constant', 'configA1', 1, 'Value A1', 'Mô tả cấu hình A1', 'Description for config A1', 'enabled', 'ownerA@example.com', NOW(), NOW(), 'Config A1 note'),
(2, 'message', 'configA2', 1, 'Value A2', 'Mô tả cấu hình A2', 'Description for config A2', 'enabled', 'ownerA@example.com', NOW(), NOW(), 'Config A2 note'),
(3, 'remote', 'configB1', 2, 'Value B1', 'Mô tả cấu hình B1', 'Description for config B1', 'enabled', 'ownerB@example.com', NOW(), NOW(), 'Config B1 note'),
(4, 'constant', 'configB2', 2, 'Value B2', 'Mô tả cấu hình B2', 'Description for config B2', 'enabled', 'ownerB@example.com', NOW(), NOW(), 'Config B2 note'),
(5, 'message', 'configC1', 3, 'Value C1', 'Mô tả cấu hình C1', 'Description for config C1', 'enabled', 'ownerC@example.com', NOW(), NOW(), 'Config C1 note'),
(6, 'remote', 'configC2', 3, 'Value C2', 'Mô tả cấu hình C2', 'Description for config C2', 'enabled', 'ownerC@example.com', NOW(), NOW(), 'Config C2 note'),
(7, 'remote', 'configC3', 3, 'Value C2', 'Mô tả cấu hình C2', 'Description for config C2', 'enabled', 'ownerC@example.com', NOW(), NOW(), 'Config C2 note'),

(8, 'remote', 'configC4', 1, 'Value C2', 'Mô tả cấu hình C2', 'Description for config C2', 'enabled', 'ownerC@example.com', NOW(), NOW(), 'Config C2 note'),
(9, 'remote', 'configC5', 1, 'Value C2', 'Mô tả cấu hình C2', 'Description for config C2', 'enabled', 'ownerC@example.com', NOW(), NOW(), 'Config C2 note'),
(10, 'remote', 'configC6', 1, 'Value C2', 'Mô tả cấu hình C2', 'Description for config C2', 'enabled', 'ownerC@example.com', NOW(), NOW(), 'Config C2 note'),
(11, 'remote', 'configC7', 1, 'Value C2', 'Mô tả cấu hình C2', 'Description for config C2', 'enabled', 'ownerC@example.com', NOW(), NOW(), 'Config C2 note'),
(12, 'remote', 'configC8', 1, 'Value C2', 'Mô tả cấu hình C2', 'Description for config C2', 'enabled', 'ownerC@example.com', NOW(), NOW(), 'Config C2 note'),
(13, 'remote', 'configC9', 1, 'Value C2', 'Mô tả cấu hình C2', 'Description for config C2', 'enabled', 'ownerC@example.com', NOW(), NOW(), 'Config C2 note'),
(14, 'remote', 'configC10', 1, 'Value C2', 'Mô tả cấu hình C2', 'Description for config C2', 'enabled', 'ownerC@example.com', NOW(), NOW(), 'Config C2 note'),
(15, 'remote', 'config11', 1, 'Value C2', 'Mô tả cấu hình C2', 'Description for config C2', 'enabled', 'ownerC@example.com', NOW(), NOW(), 'Config C2 note'),
(16, 'remote', 'configC12', 1, 'Value C2', 'Mô tả cấu hình C2', 'Description for config C2', 'enabled', 'ownerC@example.com', NOW(), NOW(), 'Config C2 note'),
(17, 'remote', 'configC21', 1, 'Value C2', 'Mô tả cấu hình C2', 'Description for config C2', 'enabled', 'ownerC@example.com', NOW(), NOW(), 'Config C2 note');
